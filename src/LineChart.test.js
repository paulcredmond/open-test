import React from "react";
import { render } from "@testing-library/react";
import { Months } from "./Consts";
import {
  LineGraph,
  groupByBirthday,
  getCumulative,
  formatData
} from "./LineChart";

const FakeUsers = [
  {
    id: 0,
    birthday: 11,
    spend: 123,
    region: "APAC",
    gender: "Female"
  },
  {
    id: 1,
    birthday: 5,
    spend: 3212,
    region: "Latin America",
    gender: "Male"
  },
  {
    id: 2,
    birthday: 2,
    spend: 3241,
    region: "United States",
    gender: "Female"
  },
  {
    id: 3,
    birthday: 5,
    spend: 1263,
    region: "United States",
    gender: "Male"
  }
];

const Cumulative = [2, 4, 6, 7, 3];

describe("Graph renders", () => {
  it("renders", () => {
    const { container } = render(
      <LineGraph users={FakeUsers} months={Months} />
    );
    expect(container).toMatchSnapshot();
  });
});

describe("Tests functions", () => {
  it("checks birthday numbers", () => {
    const result = groupByBirthday(FakeUsers);
    expect(result[0]).toEqual(0);
    expect(result[1]).toEqual(1);
    expect(result[4]).toEqual(2);
  });

  it("checks cumulative numbers", () => {
    const result = getCumulative(Cumulative);
    expect(result[0]).toEqual(10);
    expect(result[1]).toEqual(30);
    expect(result[2]).toEqual(60);
  });

  it("checks graph numbers", () => {
    const birthdays = groupByBirthday(FakeUsers);
    const cumulative = getCumulative(birthdays);
    const result = formatData(Months, birthdays, cumulative);
    expect(result[5].month).toEqual("June");
    expect(result[5].cumulative).toEqual(15);
    expect(result[11].cumulative).toEqual(20);
  });
});
