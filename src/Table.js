import React from "react";
import { Table } from "antd";
import "antd/dist/antd.css";

export const DataTable = props => {
  const { columns, users } = props;

  return <Table columns={columns} dataSource={users} rowKey="id" />;
};
