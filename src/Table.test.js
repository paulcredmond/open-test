import React from "react";
import { render } from "@testing-library/react";
import { Columns } from "./Consts";
import { DataTable } from "./Table";

const FakeUsers = [
  {
    id: 0,
    birthday: 12,
    spend: 123,
    region: "APAC",
    gender: "Female"
  },
  {
    id: 1,
    birthday: 5,
    spend: 3212,
    region: "Latin America",
    gender: "Male"
  },
  {
    id: 2,
    birthday: 2,
    spend: 3241,
    region: "United States",
    gender: "Female"
  }
];

describe("Table renders", () => {
  it("renders", () => {
    const { container } = render(
      <DataTable users={FakeUsers} columns={Columns} />
    );
    expect(container).toMatchSnapshot();
  });

  it("renders data", () => {
    const { getByText } = render(
      <DataTable users={FakeUsers} columns={Columns} />
    );
    expect(getByText("Male")).toBeInTheDocument();
  });
});
