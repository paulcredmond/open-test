import React from "react";
import { Select, Radio, Slider, InputNumber, Row, Col } from "antd";
import { throttle } from "lodash";

const { Option } = Select;

export const Filters = props => {
  const { regions, filters, updateFilters } = props;

  const onChangeSpend = value => {
    let newValue = value || 0;
    updateFilters({ spend: newValue });
  };

  const onChangeRegion = value => {
    updateFilters({ region: value });
  };

  const onChangeGender = e => {
    updateFilters({ gender: e.target.value });
  };

  return (
    <div className="Filters">
      <Row type="flex" align="middle" justify="center">
        <Col span={8}>
          <label className="Filter-label">Spend</label>
        </Col>
        <Col span={4}>
          <label className="Filter-label">Region</label>
        </Col>
        <Col span={4}>
          <label className="Filter-label">Gender</label>
        </Col>
      </Row>
      <Row type="flex" align="middle" justify="center">
        <Col span={6}>
          <Slider
            min={0}
            max={5000}
            data-testid="spend-slider"
            onChange={throttle(onChangeSpend, 100)}
            value={filters.spend}
          />
        </Col>
        <Col span={2}>
          <InputNumber
            min={0}
            max={5000}
            data-testid="spend-input"
            style={{ marginLeft: 16 }}
            value={filters.spend}
            onChange={throttle(onChangeSpend, 100)}
          />
        </Col>
        <Col span={4}>
          <Select
            style={{ width: 120 }}
            defaultValue={"All"}
            data-testid="region-dropdown"
            onChange={onChangeRegion}
          >
            <Option value="All">All</Option>
            {regions.map(region => (
              <Option key={region} value={region}>
                {region}
              </Option>
            ))}
          </Select>
        </Col>
        <Col span={4}>
          <Radio.Group onChange={onChangeGender} defaultValue="Both">
            <Radio.Button value="Male">Male</Radio.Button>
            <Radio.Button value="Female">Female</Radio.Button>
            <Radio.Button value="Both">Both</Radio.Button>
          </Radio.Group>
        </Col>
      </Row>
    </div>
  );
};
