import React, { useState, useEffect } from "react";
import { Columns, Users, Regions, Months } from "./Consts";
import { Filters } from "./Filters";
import { LineGraph } from "./LineChart";
import { DataTable } from "./Table";
import "./App.css";

const logo = "&Open";

export const App = () => {
  const [userData, setUserData] = useState(Users);
  const [filters, setFilters] = useState({
    spend: 2500,
    region: "All",
    gender: "Both"
  });

  const updateFilters = newValue => {
    setFilters({
      ...filters,
      ...newValue
    });
  };

  const filterResults = () => {
    let filteredUsers = Users.filter(item => item.spend >= filters.spend);

    if (filters.region !== "All") {
      filteredUsers = filteredUsers.filter(
        item => item.region === filters.region
      );
    }

    if (filters.gender !== "Both") {
      filteredUsers = filteredUsers.filter(
        item => item.gender === filters.gender
      );
    }

    setUserData(filteredUsers);
  };

  useEffect(() => {
    filterResults();
  }, [filters]);

  return (
    <div className="App">
      <h1>{`${logo} Technical Test`}</h1>
      <Filters
        filters={filters}
        updateFilters={updateFilters}
        regions={Regions}
      />
      <h4>Filtered Users: {userData.length}</h4>
      <LineGraph users={userData} months={Months} />
      <DataTable users={userData} columns={Columns} />
    </div>
  );
};
