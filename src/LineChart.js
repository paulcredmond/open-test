import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  CartesianGrid
} from "recharts";

export const groupByBirthday = arr => {
  const newArr = arr.reduce(
    (acc, item) => {
      const { birthday } = item;
      acc[birthday - 1]++;
      return acc;
    },
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  );
  return newArr;
};

export const getCumulative = arr => {
  const newArr = arr.reduce((r, a) => {
    r.push(((r.length && r[r.length - 1]) || 0) + a * 5);
    return r;
  }, []);
  return newArr;
};

export const formatData = (months, birthday, cumulative) => {
  const newArr = months.map((item, i) => {
    let newObj = {
      month: item,
      count: birthday[i],
      cumulative: cumulative[i]
    };
    return newObj;
  });
  return newArr;
};

export const LineGraph = props => {
  const { users, months } = props;

  const byBirthday = groupByBirthday(users);
  const byCumulative = getCumulative(byBirthday);
  const allData = formatData(months, byBirthday, byCumulative);

  return (
    <LineChart
      width={1200}
      height={500}
      data={allData}
      margin={{ top: 5, right: 5, left: 5, bottom: 30 }}
    >
      <Line
        type="monotone"
        name="Monthly"
        yAxisId="left"
        dataKey="count"
        activeDot={{ r: 4 }}
        stroke="#8884d8"
      />
      <YAxis
        yAxisId="left"
        type="number"
        domain={[0, Math.ceil(allData.length / 100) * 100]}
      />
      <Line
        type="monotone"
        name="Cumulative"
        yAxisId="right"
        dataKey="cumulative"
        activeDot={{ r: 4 }}
        stroke="#82ca9d"
      />
      <YAxis
        yAxisId="right"
        orientation="right"
        type="number"
        domain={[0, Math.ceil(allData.length / 100) * 100]}
      />
      <XAxis type="category" dataKey="month" />
      <CartesianGrid strokeDasharray="3 3" />
      <Legend verticalAlign="top" />
      <Tooltip />
    </LineChart>
  );
};
